class CreateCardDeck < ActiveRecord::Migration
  def change
    create_table :cards_decks, :id => false do |t|
          t.references :card
          t.references :deck
    end
  end
end
